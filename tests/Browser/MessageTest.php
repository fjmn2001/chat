<?php

namespace Tests\Browser;

use App\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class MessageTest extends DuskTestCase
{

    use DatabaseMigrations;

    function test_a_user_can_send_a_message_and_other_user_see_it()
    {
        $user1 = factory(User::class)->create([
            'name' => 'John Doe'
        ]);

        $user2 = factory(User::class)->create([
            'name' => 'Francisco Marcano'
        ]);

        $this->browse(function (Browser $first, Browser $second) use ($user1, $user2) {
            $first->loginAs($user1)
                ->visit('/chat')
                ->waitFor('.chatroom');

            $second->loginAs($user2)
                ->visit('/chat')
                ->waitFor('.chatroom')
                ->type('#message', 'Hey Taylor')
                ->press('Send');

            $first->waitForText('Hey Taylor')
                ->assertSee('Francisco Marcano');
        });
    }
}
